<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home.index', 'uses' => 'HomeController@index']);

Route::post('/logout', ['as' => 'user.logout', 'uses' => 'UserController@logout']);

Route::get('/article/detail/{id}', ['as' => 'article.detail', 'uses' => 'ArticleController@detail']);

Route::middleware(['auth'])->group(function () {
    Route::get('/article/manager', ['as' => 'article.index', 'uses' => 'ArticleController@index']);
    Route::get('/article/create', ['as' => 'article.create', 'uses' => 'ArticleController@create']);
    Route::get('/article/edit', ['as' => 'article.edit', 'uses' => 'ArticleController@edit']);
    Route::post('/article/edit', ['as' => 'article.edit.post', 'uses' => 'ArticleController@save_article']);
    Route::post('/article/change-status', ['as' => 'article.change_status', 'uses' => 'ArticleController@change_status']);
});

Route::middleware(['guest'])->group(function () {
    Route::get('/login', ['as' => 'user.login', 'uses' => 'UserController@login']);
    Route::post('/login', ['as' => 'user.login.post', 'uses' => 'UserController@auth']);
});