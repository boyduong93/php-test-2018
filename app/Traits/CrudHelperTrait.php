<?php

namespace App\Traits;

trait CrudHelperTrait {
    
    public function updateOrCreate($id, $data = array()) {
        $model = $this->makeModel();
        return $model->updateOrCreate(['id' => $id], $data);
    }
    
    public function update($id, $data = array()) {
        $item = $this->find($id);
        
        return $item->update($data);
    }
    
    public function find($id) {
        $model = $this->makeModel();
        
        return $model->find($id);
    }
    
    public function paginate($perPage, $where = array(), $orderBy = array(), $inputs = array()) {
        $perPage = intval($perPage);
        
        $model = $this->makeModel();
        $model = $model->where($where);
        foreach ($orderBy as $key => $value) {
            $model = $model->orderBy($key, $value);
        }
        
        $list = $model->paginate($perPage);
        $list->appends($inputs);
        
        return $list;
    }
    
    public function makeModel() {
        $className = $this->model();
        return new $className();
    }
}
