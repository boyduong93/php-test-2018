<?php

namespace App\Helper;

use App\Models\User;
use App\Traits\CrudHelperTrait;

class UserHelper {
    
    use CrudHelperTrait;
    
    public function login($email, $password) {
        $user = User::where('email', $email)->first();
        
        if ($user && $user->password == $this->encrypt_password($password, $user->salt)) {
            return $user;
        }
        
        return null;
    }
    
    public function encrypt_password($password, $salt) {
        return md5(base64_encode($password . $salt));
    }
    
    public function model() {
        return User::class;
    }
    
}