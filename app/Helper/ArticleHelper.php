<?php

namespace App\Helper;

use App\Models\Article;
use App\Traits\CrudHelperTrait;

class ArticleHelper {
    
    use CrudHelperTrait;
    
    public function model() {
        return Article::class;
    }
    
}
