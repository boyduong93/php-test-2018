<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements AuthenticatableContract {

    use Authenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'email', 'password', 'picture', 'salt'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public static $rule_auth = [
        'email' => 'required|email',
        'password' => 'required'
    ];
    
    public static $rule = [
        'fullname' => 'required',
        'email' => 'required|email|unique:users,email',
        'password' => 'required',
        'salt' => 'required'
    ];

    public function articlies() {
        return $this->hasMany('App\Models\Article', 'creator', 'id');
    }

}
