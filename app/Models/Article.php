<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Michelf\Markdown;

class Article extends Model {

    protected $table = 'articlies';
    
    protected $dates = [
        'created_at', 'updated_at'
    ];
    
    protected $fillable = [
        'title', 'body', 'creator', 'status'
    ];
    
    public static $rule = [
        'title' => 'required|unique:articlies,title',
        'creator' => 'integer|exists:users,id',
        'status' => 'required|integer|in:0,1,2'
    ];
    
    public static $status_pending = 0;
    public static $status_deactive = 1;
    public static $status_active = 2;
    
    public static $mapping_status = [
        0 => 'Pending',
        1 => 'Deactive',
        2 => 'Active'
    ];
    
    public function bodyHTML() {
        return Markdown::defaultTransform($this->body);
    }
    
    public function creator() {
        return $this->belongsTo('App\Models\User', 'creator', 'id');
    }
    
    public function creatorFullname() {
        $creator = $this->creator()->first();
        if ($creator) {
            return $creator->fullname;
        }
    }

}
