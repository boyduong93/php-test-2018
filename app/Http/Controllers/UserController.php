<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Helper\UserHelper;
use Session;
use Validator;
use App\Models\User;

class UserController extends Controller {

    public function logout (Request $request) {
        if (Auth::guard()->check()) {
            Auth::guard()->logout();
        }

        return redirect()->route('home.index');
    }
    
    public function login (Request $request) {
        return view('user.login')->with('error', false);
    }
    
    public function auth (Request $request) {
        $errorMessage = 'Email or password is incorrect.';
        
        $email = $request->get('email');
        $password = $request->get('password');
        $remember = $request->get('remember');
        
        $validator = Validator::make(['email' => $email, 'password' => $password], User::$rule_auth);
        if ($validator->fails()) {
            $request->flash();
            $errorMessage = $validator->getMessageBag()->first();
            return view('user.login')->with('error', $errorMessage);
        }
        
        $userHelper = new UserHelper();
        $user = $userHelper->login($email, $password);
        if ($user) {
            Auth::guard()->login($user, $remember == 1);
            if (Session::has('url.intended')) {
                $path = Session::get('url.intended');
                Session::forget('url.intended');
                
                return redirect()->to($path);
            }
            return redirect()->route('home.index');
        }
        
        $request->flash();
        return view('user.login')->with('error', $errorMessage);
    }

}
