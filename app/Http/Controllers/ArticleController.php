<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\ArticleHelper;
use Validator;
use App\Models\Article;
use Auth;

class ArticleController extends Controller {

    public function index (Request $request) {
        $articleHelper = new ArticleHelper();
        
        $keyword = $request->get('keyword');
        $perPage = $request->get('perPage', 10);
        
        $where = array();
        $orderBy = $request->get('orderBy', 'id:desc');
        if ($keyword) {
            $where[] = array('title', 'like', '%' . $keyword . '%');
        }
        
        if ($orderBy) {
            $orderBy = explode(':', $orderBy);
            $orderBy = array($orderBy[0] => $orderBy[1]);
        }
        $orderByString = isset($orderBy) && count(array_keys($orderBy)) > 0 ? (array_keys($orderBy)[0] . ':' . array_values($orderBy)[0]) : '';
        
        $articlies = $articleHelper->paginate($perPage, $where, $orderBy, $request->input());
        
        return view('article.index')->with('articlies', $articlies)->with('perPage', $perPage)
                ->with('keyword', $keyword)->with('orderBy', $orderBy)->with('orderByString', $orderByString);
    }
    
    public function change_status (Request $request) {
        $id = $request->get('id');
        $status = $request->get('status');
        $articleHelper = new ArticleHelper();
        
        $article = $articleHelper->update($id, array('status' => $status));
        
        return response()->json(['success' => true]);
    }
    
    public function save_article (Request $request) {
        $errorMessage = 'Please try again.';
        
        $articleHelper = new ArticleHelper();
        
        $id = $request->get('id');
        $title = $request->get('title');
        $body = $request->get('body');
        
        $rules = Article::$rule;
        $is_new = true;
        $article = array();
        $status = Article::$status_pending;
        $creator = Auth::guard()->user()->id;
        if ($id) {
            $article = $articleHelper->find($id);
            if ($article) {
                $rules['title'] = $rules['title'] . ',' . $article->id . ',id';
                $is_new = false;
                $status = $article->status;
                $creator = $article->creator;
            }
        }
        
        $validator = Validator::make(['title' => $title, 'creator' => $creator, 'status' => $status], $rules);
        
        if ($validator->fails()) {
            $request->flash();
            $errorMessage = $validator->getMessageBag()->first();
            return view('article.edit')->with('is_new', $is_new)->with('article', $article)->with('error', $errorMessage);
        }
        
        $article = $articleHelper->updateOrCreate($id, array('title' => $title, 'body' => $body, 'creator' => $creator, 'status' => $status));
        if ($article) {
            return redirect()->route('article.index');
        }
        
        $request->flash();
        return view('article.edit')->with('is_new', $is_new)->with('article', $article)->with('error', $errorMessage);
    }
    
    public function create (Request $request) {
        $article = array();
        $is_new = true;
        
        return view('article.edit')->with('article', $article)->with('is_new', $is_new)->with('error', false);
    }
    
    public function edit (Request $request) {
        $id = $request->get('id');
        $articleHelper = new ArticleHelper();
        
        $article = $articleHelper->find($id);
        $is_new = false;
        
        return view('article.edit')->with('article', $article)->with('is_new', $is_new)->with('error', false);
    }
    
    public function detail (Request $request, $id = null) {
        $articleHelper = new ArticleHelper();
        
        $article = $articleHelper->find($id);
        
        return view('article.detail')->with('article', $article);
    }

}
