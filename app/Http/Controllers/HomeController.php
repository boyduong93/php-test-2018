<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\ArticleHelper;
use App\Models\Article;

class HomeController extends Controller {
    
    public function index (Request $request) {
        $articleHelper = new ArticleHelper();
        
        $perPage = $request->get('perPage', 10);
        
        $articlies = $articleHelper->paginate($perPage, ['status' => Article::$status_active], ['id' => 'desc'],$request->input());
        
        return view('home.index')->with('articlies', $articlies);
    }

}
