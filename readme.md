## Setup project

- Require system: Ubuntu 16.04, Nginx, PHP 7.0, MYSQL

- Clone: git clone https://bitbucket.org/boyduong93/php-test-2018.git
- Composer install: sudo composer install
- Create database: create database mysql name: pink
- Database install: php artisan migrate:refresh --seed
- Setup .env file require (APP_KEY, APP_URL, DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, DB_PASSWORD)
- Run project

- Homepage: /
- Article manager: /article/manager
- User login: /login
- Email: duongbkap@gmail.com
- Password: 123456