<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Article Detail</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <h4><b>Title: {{ $article->title }}</b></h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            Created at: {{ $article->created_at->format('M d, Y H:i:s') }}
        </div>
        <div class="col-md-6">
            Updated at: {{ $article->updated_at->format('M d, Y H:i:s') }}
        </div>
    </div>
    <div class="row margin-top-10">
        <div class="col-md-12">
            {!! $article->bodyHTML() !!}
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
</div>