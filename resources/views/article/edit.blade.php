@extends('/layouts/home')
@section('css')
<link href="{{ asset('/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
<script src="{{ asset('/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    validate_form_article_create();
    @if ($error)
        App.alert({
            message: '{{ $error }}',
            type: 'danger'
        });
    @endif
</script>
@stop

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Article
                    <small>create</small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('home.index') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Article</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark bold uppercase">Article info</span>
                            <span class="caption-helper"></span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form id='form_article_create' method="post" action="{{  route('article.edit.post') }}" class="form-horizontal form-bordered">
                            {!! csrf_field() !!}
                            
                            <input type="hidden" name='id' value="{{ !$is_new ? $article->id : '' }}" />
                            
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Title <span class="required"> * </span></label>
                                    <div class="col-md-10">
                                        <input value="{{ old('title', !$is_new ? $article->title : '') }}" data-required="1" type="text" class="form-control" name="title" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-2">Body</label>
                                    <div class="col-md-10">
                                        <textarea name="body" data-provide="markdown" rows="15">{!! old('body', !$is_new ? $article->body : '') !!}</textarea>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-2 col-md-10">
                                        <button type="submit" class="btn dark">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <a href="{{ route('article.index') }}" class="btn default">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop