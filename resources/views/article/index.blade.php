@extends('/layouts/home')
@section('css')
<link href="{{ asset('/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
    .table td {
        vertical-align: middle !important;
    }
    .portlet-title {
        padding: 0 10px !important;
    }
    .portlet-title .caption {
        color: #3598dc;
    }
    .portlet-title .actions a {
        padding: 10px 9px !important;
        margin: 0 0 4px;
    }
</style>
@stop

@section('js')
<script src="{{ asset('/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    function change_status (id, select) {
        var status = select.value;
        var old_status = $(select).data('old-value');
        
        App.blockUI();
        
        var message_error = 'Change status error.';
        var message_success = 'Change status success.';
        
        $.ajax({
            type: 'POST',
            url: '{{ route("article.change_status", ["id" => ""]) }}' + id,
            dataType: 'json',
            data: {status: status, _token: '{{ csrf_token() }}'},
            success: function (response, textStatus, jqXHR) {
                if (jqXHR.status != 200) {
                    select.value = old_status;
                    App.alert({
                        type: 'danger',
                        message: message_error,
                        closeInSeconds: 3
                    });
                    return;
                }
                
                App.alert({
                    type: 'success',
                    message: message_success,
                    closeInSeconds: 3
                });
                
                $(select).data('old-value', status);
            },
            error: function (jqXHR, ajaxOptions, thrownError) {
                App.unblockUI();
                select.value = old_status;
                App.alert({
                    type: 'danger',
                    message: message_error,
                    closeInSeconds: 3
                });
            }
        }).done(function () {
            App.unblockUI();
        });
    }
    
    function change_orderby (key, order) {
        _change_orderby('{!! route("article.index", ["keyword" => $keyword, "perPage" => $perPage, "orderBy" => ""]) !!}', key, order);
    }
    
    function change_per_page (val) {
        _change_per_page('{!! route("article.index", ["keyword" => $keyword, "orderBy" => $orderByString, "perPage" => ""]) !!}', val);
    }
    
    _init_table('sample_3', {!! json_encode($orderBy) !!}, '{!! route("article.index", ["perPage" => $perPage, "orderBy" => $orderByString, "keyword" => ""]) !!}', '{!! route("article.index", ["perPage" => $perPage, "keyword" => $keyword, "orderBy" => ""]) !!}');
    
    $(".select2").select2();
</script>
@stop

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Article
                    <small>manager</small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('home.index') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Article</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark bold uppercase">Article lists</span>
                            <span class="caption-helper"></span>
                        </div>
                        <div class="actions">
                            <div class="btn-group">
                                <a class="btn green btn-outline btn-circle btn-sm uppercase sbold" href="{{ route('article.create') }}"> Add new</a>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="dataTables_wrapper no-footer">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="dataTables_length">
                                        <label>
                                            <select onchange="change_per_page(this.value)" class="form-control input-xsmall input-inline select2">
                                                @foreach ([5, 10, 20, 50, 100] as $page)
                                                    <option value="{{ $page }}" {!! $page == $perPage ? 'selected="selected"' : '' !!}>{{ $page }}</option>
                                                @endforeach
                                            </select>
                                            records </label>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="dataTables_filter">
                                        <label>Search:
                                            <input value="{{ $keyword }}" id="table_search" type="search" class="form-control input-small input-inline">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="table-scrollable">
                                <table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_3">
                                    <thead>
                                        <tr>
                                            <th data-orderby="title">Title</th>
                                            <th data-orderby="creator" class="text-center">Creator</th>
                                            <th data-orderby="status" class="text-center">Status</th>
                                            <th data-orderby="created_at" class="text-center">Created at</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if ($articlies->count() == 0)
                                            <tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No data available in table</td></tr>
                                        @endif
                                        @foreach ($articlies as $article)
                                            <tr>
                                                <td>{{ $article->title }}</td>
                                                <td class="text-center">{{ $article->creatorFullname() }}</td>
                                                <td class="text-center">
                                                    <select data-old-value="{{ $article->status }}" onchange="change_status('{{ $article->id }}', this)">
                                                        @foreach ([App\Models\Article::$status_pending, App\Models\Article::$status_deactive, App\Models\Article::$status_active] as $status)
                                                        <option value='{{ $status }}' {!! $status == $article->status ? 'selected="selected"' : '' !!}>{{ App\Models\Article::$mapping_status[$status] }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td class="text-center">{{ $article->created_at->format('M d, Y') }}</td>
                                                <td class="text-center">
                                                    <a href="{{ route('article.edit', ['id' => $article->id]) }}" class="btn btn-primary">Edit</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-sm-12">
                                    @php $start = ($articlies->currentPage() - 1) * $articlies->perPage(); @endphp
                                    @php $start = $start > $articlies->total() - 1 ? $articlies->total() - 1 : $start; @endphp
                                    <div class="dataTables_info">Showing {{ $start  + 1 }} to {{ $start + $articlies->perPage() < $articlies->total() ? $start + $articlies->perPage() : $articlies->total() }} of {{ $articlies->total() }} entries</div>
                                </div>
                                <div class="col-md-7 col-sm-12">
                                    <div class="dataTables_paginate paging_simple_numbers">
                                        {!! $articlies->render() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop