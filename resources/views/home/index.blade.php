@extends('/layouts/home')
@section('css')
<style type="text/css">
    .blog-content-1 .blog-post-content>.blog-post-desc {
        height: 38px;
    }
</style>
@stop

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Article
                    <small>article listing page</small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('home.index') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Article</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="blog-page blog-content-1">
            <div class="row pull-left">
                <div class="col-lg-12">
                    {!! $articlies->render() !!}
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        @foreach ($articlies as $article)
                        <div class="col-sm-6">
                            <div class="blog-post-sm bordered blog-container">
                                <div class="blog-post-content">
                                    <h2 class="blog-title blog-post-title">
                                        <a data-target="#ajax_content" data-toggle="modal" href="{{ route('article.detail', ['id' => $article->id]) }}">{{ $article->title }}</a>
                                    </h2>
                                    <p class="blog-post-desc"> {{ str_limit(strip_tags($article->bodyHTML()), 200) }} </p>
                                    <div class="blog-post-foot">
                                        <div class="blog-post-meta">
                                            <i class="icon-calendar font-blue"></i>
                                            <a href="javascript:;">{{ $article->created_at->format('M d, Y') }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        
                    </div>
                </div>
            </div>
            <div class="row pull-right">
                <div class="col-lg-12">
                    {!! $articlies->render() !!}
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop