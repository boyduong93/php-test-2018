<?php

use Faker\Generator as Faker;
use App\Helper\UserHelper;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$userHelper = new UserHelper();

$factory->define(User::class, function (Faker $faker) use ($userHelper) {
    $salt = str_random(6);
    $password = '123456';
    
    return [
        'fullname' => 'Tran Hong Duong',
        'email' => 'duongbkap@gmail.com',
        'salt' => $salt,
        'picture' => 'https://graph.facebook.com/100000356154258/picture?type=large',
        'password' => $userHelper->encrypt_password($password, $salt), // secret
        'remember_token' => str_random(10),
    ];
});
