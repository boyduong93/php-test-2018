<?php

use Faker\Generator as Faker;
use App\Models\Article;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Article::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->sentence(100),
        'creator' => 1,
        'status' => array_random([Article::$status_pending, Article::$status_deactive, Article::$status_active])
    ];
});
