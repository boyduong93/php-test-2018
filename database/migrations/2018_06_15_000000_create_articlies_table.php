<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articlies', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->string('title', 1000);
            $table->text('body');
            $table->unsignedInteger('creator')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
            
            $table->foreign('creator')->references('id')->on('users')->onUpdate('cascade')->onDelete('set null');
            
            $table->unique(array('title'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articlies');
    }
}
