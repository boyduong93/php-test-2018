<?php

use Illuminate\Database\Seeder;

class ArticliesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        factory(App\Models\Article::class, 1000)->create();
    }

}
