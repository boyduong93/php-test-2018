$.fn.enterKey = function (fnc) {
    return this.each(function () {
        $(this).keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            if (keycode == 13) {
                fnc.call(this, ev);
            }
        });
    });
}; 

function _init_table (id, orderBy, url_search, url_orderby) {
    $("#" + id).parents('.dataTables_wrapper').find('input[type="search"]').enterKey(function () {
        var keyword = $(this).val();
        var url = url_search + keyword;
        window.location.href = url;
    });
    
    $('#' + id + ' [data-orderby]').each(function (index, elm) {
        var key = $(elm).data('orderby');
        var order = 'asc';
        if (Object.keys(orderBy).indexOf(key) != -1) {
            $(elm).addClass('sorting_' + orderBy[key]);
            if (orderBy[key] == 'asc') {
                order = 'desc';
            }
        } else {
            $(elm).addClass('sorting');
        }
        
        $(elm).on('click', function (event) {
            _change_orderby(url_orderby, key, order);
        });
    });
}

function _change_per_page (url, page) {
        url += page;
        window.location.href = url;
    }

function _change_orderby (url, key, order) {
    var orderBy = key + ":" + order;
    url += orderBy;
    window.location.href = url;
}

$('#btn-logout').on('click', function (event) {
    var form = $(this).find('form');
    form.submit();
});

function validate_form_article_create () {
    var form1 = $('#form_article_create');
    var error1 = $('.alert-danger', form1);
    var success1 = $('.alert-success', form1);
    form1.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
            title: {
                required: true
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success1.hide();
            error1.show();
            App.scrollTo(error1, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            if (cont && cont.length > 0) {
                cont.after(error);
            } else {
                element.after(error);
            }
        },

        highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label.closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function (form) {
            success1.show();
            error1.hide();
            form.submit();
        }
    });
}